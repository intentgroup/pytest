import requests
import threading
import configparser
from random import randint

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from .bot_queries import *
from .bot_functions import (
    verify_email,
    get_all_posts,
    generate_likes,
    generate_posts,
    get_user_token,
    get_additional_data_from_email,
)


class Command(BaseCommand):
    help = 'Bot to demonstrate how the API is used.'

    def handle(self, *args, **kwargs):
        User = get_user_model()

        config = configparser.ConfigParser()
        config.read("bot_config.ini")
        number_of_users = int(config['BOT_CONFIG']['number_of_users'])
        max_posts_per_user = int(config['BOT_CONFIG']['max_posts_per_user'])
        max_likes_per_user = int(config['BOT_CONFIG']['max_likes_per_user'])
        clearbit_api_key = config['BOT_CONFIG']['clearbit_api_key']
        hunter_api_key = config['BOT_CONFIG']['hunter_api_key']

        emails = [
            'customerservices@theconranshop.com',
            'infoact@totalmobility.com.au',
            'sales@bettermobility.co.uk',
            'hello@mobilityshop.co.uk',
            'aamobility@btconnect.com',
            'info@pridemobility.com',
            'help@signvideo.co.uk',
        ]
        threads = []

        if number_of_users > len(emails):
            print('Not enough emails. NUMBER_OF_USERS = len(EMAILS)')
            number_of_users = len(emails)

        def create_user_with_posts_and_likes(email):
            verified_email = verify_email(email, hunter_api_key)
            if verified_email:
                user_data = get_additional_data_from_email(
                    verified_email, clearbit_api_key
                )

                user_responce = requests.post(url=url_users, data=user_data)
                user_id = user_responce.json()['id']
                username = user_data['username']
                password = user_data['password']

                random_posts_count = randint(1, max_posts_per_user) + 1
                print(f'For user {username} generating {random_posts_count} posts')

                token = get_user_token(url_get_token, username, password)
                for _ in range(1, random_posts_count):
                    generate_posts(url_posts, user_id, token)

        for i in range(1, number_of_users + 1):
            t = threading.Thread(
                target=create_user_with_posts_and_likes,
                args=(emails[i],),
                daemon=True)
            t.start()
            print(f'Thread {t.getName()} starting...')
            threads.append(t)

        for thr in threads:
            thr.join()

        def users_perform_likes():
            print('Begin to likes...')
            users_with_posts = []
            max_likes_count = max_likes_per_user

            all_users = requests.get(url_users).json()
            for _ in all_users:
                if _['posts_count'] != 0:
                    users_with_posts.append(_)

            users_with_posts = sorted(users_with_posts,
                                      reverse=True,
                                      key=lambda u: u['posts_count'])

            for user in users_with_posts:
                user_id = user['id']
                cred = user['username']
                token = get_user_token(url_get_token, cred, cred)

                all_posts = get_all_posts(url_posts, token)
                not_owned_posts = []
                for post in all_posts:
                    if post['user'] != user['id']:
                        not_owned_posts.append((post['id'], post['likes']))

                for _ in range(1, max_likes_count + 1):
                    random_post_id = randint(1, len(not_owned_posts) + 1)
                    try:
                        if not_owned_posts[random_post_id][1] == 0:
                            generate_likes(url_likes,
                                           token=token,
                                           owner_id=user_id,
                                           post_id=
                                           not_owned_posts[random_post_id][0])
                    except IndexError:
                        random_post_id = randint(1, len(not_owned_posts) + 1)
                        if not_owned_posts[random_post_id][1] == 0:
                            generate_likes(url_likes,
                                           token=token,
                                           owner_id=user_id,
                                           post_id=
                                           not_owned_posts[random_post_id][0])

        users_perform_likes()

