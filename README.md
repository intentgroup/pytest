## Installation

python3.6

Don't forget to manage with .ini file 

```sh
cd pytest
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

cd test_django_api
./manage.py migrate
./manage.py runserver
```

To run bot use
```sh
./manage.py bot_api
```