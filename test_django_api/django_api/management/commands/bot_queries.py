HOST = 'http://127.0.0.1:8000'

GET_TOKEN = '/api-token-auth/'
POSTS = '/posts/'
USERS = '/users/'
LIKES = '/likes/'

url_users = HOST + USERS
url_posts = HOST + POSTS
url_likes = HOST + LIKES
url_get_token = HOST + GET_TOKEN
