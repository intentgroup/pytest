from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()


class Post(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField(max_length=200)
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='author'
    )

    @property
    def likes(self):
        count_likes = Like.objects.filter(post__pk=self.pk).count()
        return count_likes

    def __str__(self):
        return self.title


class Like(models.Model):
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='like_owner'
    )
    post = models.ForeignKey(
        'Post',
        on_delete=models.CASCADE,
    )
    already_liked = models.BooleanField(default=False)

    class Meta:
        unique_together = ('owner', 'post')

    def __str__(self):
        return f'{self.owner} liked {self.post}'

