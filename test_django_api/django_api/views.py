from django.contrib.auth import get_user_model

from .models import Post, Like
from .serializers import PostSerializer, LikeSerializer, UserSerializer
from rest_framework import viewsets, permissions


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class LikeViewSet(viewsets.ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    User = get_user_model()
    queryset = User.objects.all()
    serializer_class = UserSerializer
