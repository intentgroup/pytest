import json
from typing import Dict

import requests
import clearbit
from faker import Faker


fake = Faker()


def verify_email(email: str, hunter_api_key: str) -> str:
    url = f'https://api.hunter.io/v2/email-verifier?email={email}&api_key={hunter_api_key}'
    r = requests.get(url)
    if r.status_code == 200:
        json_data = json.loads(r.text)
        if json_data['data']['status'] == 'invalid':
            return ''

        return json_data['data']['email']
    return ''


def get_additional_data_from_email(email: str, clearbit_api_key: str) -> Dict:
    clearbit.key = clearbit_api_key

    email_data = clearbit.Enrichment.find(email=email, stream=True)
    user_data = {
        'username': email_data['company']['domain'],
        'email': email,
        'password': email_data['company']['domain'],
    }

    return user_data


def get_user_token(url: str, username: str, password: str) -> str:
    user_data = {
        "password": password,
        "username": username
    }

    r = requests.post(url, data=user_data)

    token = r.json()['token']

    return token


def generate_posts(url: str, user_id: int, token: str):
    post_data = {
        "title": fake.address()[:50],
        "content": fake.text()[:200],
        "user": user_id
    }
    headers = {'Authorization': f'Bearer {token}'}
    requests.post(url, post_data, headers=headers)


def generate_likes(url: str, owner_id: int, post_id: int,  token: str):
    like_data = {
        "owner": owner_id,
        "post": post_id
    }
    headers = {'Authorization': f'Bearer {token}'}
    requests.post(url, like_data, headers=headers)


def get_all_posts(url: str, token: str):
    headers = {'Authorization': f'Bearer {token}'}
    all_posts = requests.get(url, headers=headers)

    return all_posts.json()
